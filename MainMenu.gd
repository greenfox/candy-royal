extends Node

export var multiplayerMenu:NodePath
export var lobbyMenu:NodePath
export var audioPlayer:NodePath
#export var 
var menuStack = []

func _ready():
	NetworkManager.mainMenu = filename
	NetworkManager.connect("lobbyReady",self,"lobbyReady")
	yield(self,"ready")
#	yield(get_tree().create_timer(2),"timeout")
	transitionTo(get_node(multiplayerMenu),.5)
	if get_tree().has_network_peer():
		lobbyReady() #untestedz


func _process(delta):
	pass

	
func lobbyReady():
	transitionTo(get_node(lobbyMenu))
	

func transitionTo(node:Node,time=.25):
	(get_node(audioPlayer) as AudioStreamPlayer).play()
	menuStack.push_front(node)
	$Camera2D.transitionTime = time
	$Camera2D.target = node
	if node.has_method("transitionTo"):
		node.transitionTo()


func popTransition(node):
	if menuStack[0] != node:
		print('not active menu')
		return
	menuStack.pop_front()
	if menuStack.size():
		$Camera2D.target = menuStack[0]
	else:
		if OS.get_name() == "HTML5":
			return
		else:
			get_tree().quit()

func onBackButton(node):
	popTransition(node)





func _on_joinHostMenu_transitionTo(nodeName):
	match nodeName:
		"search":
			transitionTo($LobbyManager)
		_:
			pass
