tool
extends EditorPlugin

var dock
func _enter_tree():
	reset()
	

func reset():
	print("GFProject Pluggin reset")
	if dock:
		remove_control_from_docks(dock)
	dock = load("res://addons/GreenFoxsProjectPlugin/NotesPanel.tscn").instance()
	add_control_to_dock(EditorPlugin.DOCK_SLOT_RIGHT_BR,dock)
	dock.connect("reset",self,"reset")

	
func _exit_tree():
	remove_control_from_docks(dock)
	dock.free()
