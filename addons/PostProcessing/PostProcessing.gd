tool
extends MeshInstance

export var postProcessingMaterial:Material setget setMaterial

func setMaterial(newMat:Material):
	postProcessingMaterial = newMat
	material_override = postProcessingMaterial;

func _ready():
	var quad : QuadMesh = QuadMesh.new();
	quad.size = Vector2(2,2);
	mesh = quad;
	setMaterial(postProcessingMaterial)
	extra_cull_margin = 16000;
	


func toggleABTest():
	var a = (postProcessingMaterial as ShaderMaterial)
	a.set_shader_param("abTest", a.get_shader_param("abTest") == false )
