tool
extends Timer


export var ticksPerSec:int setget setTicks

func _ready():
	connect("timeout",self,"heartbeat")
	
func _physics_process(delta):
	totalDelta += delta

var totalDelta = 0.0
func heartbeat():
	get_tree().call_group("networkHeartbeat","_network_process",totalDelta)
	totalDelta = 0.0




func setTicks(value):
	value = max(1,value)
	set_wait_time(1.0 / value)
	ticksPerSec = value

puppet func getTickRate():
	pass
