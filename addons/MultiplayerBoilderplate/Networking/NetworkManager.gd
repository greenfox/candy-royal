#class_name NetworkManager #autoload singleton
extends Node

signal peerConnected(id)
signal peerDisconnected(id)

signal networkDropped
signal lobbyReady
signal localGame
signal kicked

var peer :NetworkedMultiplayerPeer

export var portNumber = 7777
export var playerMax = 4
var rand:RandomNumberGenerator =RandomNumberGenerator.new()


var mainMenu

func _ready():
	rand.randomize()
	var tree = get_tree()
	tree.connect("network_peer_connected",self,"network_peer_connected")
	tree.connect("network_peer_disconnected",self,"network_peer_disconnected")
	tree.connect("connected_to_server",self,"connected_to_server")
	tree.connect("connection_failed",self,"connection_failed")
	tree.connect("server_disconnected",self,"server_disconnected")

func host():
	Gotm.host_lobby(false)
	Gotm.lobby.name = "%s's Lobby!" % userName
	Gotm.lobby.hidden = false

	peer = NetworkedMultiplayerENet.new()
	peer.compression_mode = NetworkedMultiplayerENet.COMPRESS_ZLIB
	var error = peer.create_server(portNumber,playerMax-1)
	match error:
		ERR_CANT_CREATE:
			assert(false,"failed to create server!")
			return
		OK:
			pass
		_:
			print("unspecified error!:", error)
			return
	get_tree().set_network_peer(peer)
	emit_signal("lobbyReady")
 

func connectTo(hostname):
	peer = NetworkedMultiplayerENet.new()
	peer.compression_mode = NetworkedMultiplayerENet.COMPRESS_ZLIB
	peer.create_client(hostname, portNumber)
	get_tree().set_network_peer(peer)


func network_peer_connected(id):
	emit_signal("peerConnected",id)


func network_peer_disconnected(id):
	emit_signal("peerDisconnected",id)


func connected_to_server():
	emit_signal("lobbyReady")

func connection_failed():
	cleanup()
	#todo reset up, post error message "Failed to connect!"

func server_disconnected():
	cleanup()
	resetMenu()

func resetMenu():
	if get_tree().current_scene.filename != mainMenu:
		get_tree().change_scene(mainMenu)

var userName = "" setget ,getName
func getName()->String:
#	if userName == "":
#		userName = randomName()
	return userName

func randomName():
	return usernames[rand.randi() % usernames.size()]
	
export var usernames = ["Donut","Penguin","Stumpy","Whicker","Shadow","Howard",
"Wilshire","Darling","Disco","Jack","The Bear","Sneak","The Big L","Whisp",
"Wheezy","Crazy","Goat","Pirate","Saucy","Hambone","Butcher","Walla Walla",
"Snake","Caboose","Sleepy","Killer","Stompy","Mopey","Dopey","Weasel","Ghost",
"Dasher","Grumpy","Hollywood","Tooth","Noodle","King","Cupid","Prancer"]



func getTickDelay():
	return $Timer.wait_time #todo getter setter


func cleanup():
	peer.close_connection()
	get_tree().network_peer = null
	emit_signal("networkDropped")
	pass #todo break everything!

func lockGame():
	peer.refuse_new_connections = true

func unlockGame():
	peer.refuse_new_connections = false

func kick(id:int):
	rpc_id(id,"kicked")
	peer.disconnect_peer(id,false)

puppet func kicked():
	emit_signal("kicked")


func callOnMaster(node:Node,function:String,args=[]):
	if node.is_network_master():
		node.call(function,args)
	else:
		node.rpc(function,args)

