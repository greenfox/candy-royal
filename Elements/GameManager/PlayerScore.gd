extends MarginContainer

var player:Player = null
export var playerNamePlate:NodePath = ""
export var playerScore:NodePath = ""

export(SpawnPoint.SPAWN_LOCATION) var spawner = SpawnPoint.SPAWN_LOCATION.UNDEFINED


var score = -1

func _ready():
	visible = false


remotesync func setPlayer(newPlayer:NodePath):
	if player != null:
		player.disconnect("dataUpdated",self,"dataUpdated")
	if newPlayer == "":
		player = null
	else:
		player = get_node(newPlayer)
	if player != null:
		player.connect("dataUpdated",self,"dataUpdated")
		dataUpdated(player.data)
		visible = true
	else:
		visible = false

func dataUpdated(data:Player.PlayerData):
	var namePlate:Label= get_node(playerNamePlate)
	var scorePlate:Label = get_node(playerScore)
	namePlate.text = data.username
	namePlate.add_color_override("font_color",data.color)
	scorePlate.add_color_override("font_color",data.color)
	if score != data.score:
		score = data.score
		scorePlate.text = String(score) + " candies!"
	
