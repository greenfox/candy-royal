class_name CandyBullet
extends Area

export var speed = 1000 

var target:Spatial
var shooter:Spatial

signal hitTarget(bullet)


func _physics_process(delta):
	var t = target.global_transform.origin
	
	var diff:Vector3 = t - global_transform.origin
	var moveThisFrame = delta*speed
	if moveThisFrame < diff.length():
		global_transform.origin += diff.normalized() * moveThisFrame
	else:
		global_transform.origin = t


func _on_candyBullet_body_entered(body:PhysicsBody):
	if body == shooter:
		return #ignore shooter
	if body == target:
		emit_signal("hitTarget",self)
		hitFx(true)
		queue_free()#todo hit fx
	else:
		hitFx(false)
		queue_free()#todo miss fx


func hitFx(player:bool):
	var fx:Spatial = preload("res://Elements/hitFX.tscn").instance()
	fx.transform = transform
	if player:
		fx.hitPlayer()
	else:
		fx.hitWorld()

	get_parent().add_child(fx)
