class_name PawnStateMechine
extends StateMachine

var pawn

func _ready():
	._ready()
	yield(owner, "ready")
	pawn = get_parent()

func transition_to(state,msg={}):
	.transition_to(state,msg)
	rpc("setState",state)

var remoteState = ""
remotesync func setState(name):
	remoteState = name
