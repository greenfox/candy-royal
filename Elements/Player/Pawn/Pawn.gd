class_name Pawn
extends KinematicBody

export var gravity:Vector3= Vector3.DOWN * 9.8
var velocity:Vector3 = Vector3.ONE

export var walkSpeed:float= 1500
export var stunnedWalkSpeed :float= 250
export var stunDurration:float=0.5

var speed:Vector3 = Vector3.ZERO
var player:Player

remotesync var realRotation = 0

var bulletCount = 0

signal fire
signal controlsUsed

var stun = false

func _ready():
	if get_tree().has_network_peer() == false:
		pass #set up not networked
	else:
		player = GameManager.players[get_network_master()]
#		if is_network_master():
		player.connect("dataUpdated",self,"playerDataUpdated")
#		player.emit_signal("dataUpdated")
	
func isLocalPlayer()->bool:
	if get_tree().has_network_peer() == false:
		return true
	else:
		return is_network_master()

func playerDataUpdated(data:Player.PlayerData):
	$Wolf.shirtColor = data.color

func _process(delta):
	$Wolf.rotation.y = lerp_angle($Wolf.rotation.y,realRotation,0.5)
#	print(realRotation)



func getInput()->Controls:
	var controls = Controls.new().getInput()
	if controls.direction != Vector2.ZERO:
		var angle = (controls.direction*Vector2(1,-1)).angle()
		if angle != realRotation:
			rset_unreliable("realRotation",angle)
		emit_signal("controlsUsed")
	return controls


func setCrossHairTarget(body):
	pass # Replace with function body.

master func scorePoint(value):
	get_parent().add_child(preload("res://Elements/Player/candySound.tscn").instance())
	player.scorePoint(value)


func fire():
	if $targeter.currentTarget != null and player.data.score >= 1:
		player.data.score -= 1
		player.dataUpdated()
		emit_signal("fire",self,$targeter.currentTarget)
		
		
func nextBulletName()->String:
	var bulletName = "%s_%d" %[name,bulletCount]
	bulletCount += 1
	return bulletName

const hitDrops = 4
func candyHit()->int:
	if $StateMechine.remoteState == "Stun":
		player.data.score += 1
		player.dataUpdated()
		return 0
	else:
		var drop = min(player.data.score,hitDrops)
		player.data.score += 1 - drop
		player.dataUpdated()
		NetworkManager.callOnMaster(self,"stun")
		return drop


remotesync func hitFX():
	pass

remote func stun(args=[]):
	stun = true
	$StateMechine.transition_to("Stun")
