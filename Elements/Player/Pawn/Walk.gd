extends State

#export var speed = 2500

func physics_update(delta):
	var c = pawn.getInput()
	
	
	var vec3dir = Vector3(c.direction.x,0,c.direction.y).normalized()
	var outspeed = pawn.move_and_slide(vec3dir*delta*pawn.walkSpeed + (pawn.gravity))
	outspeed.y = 0
		
	if pawn.translation.y < -10:
		pawn.translation = Vector3(0,10,0)

	if c.fire:
		pawn.fire()
