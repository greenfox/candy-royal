class_name Controls

var fire:bool = false

var direction:Vector2 = Vector2.ZERO
func to_string():
	return {d=direction}

func getInput()->Controls:
	direction = Vector2.ZERO
	
	fire = Input.is_action_just_pressed("fire")
	
	if Input.is_action_pressed("up"):
		direction += Vector2.UP
	if Input.is_action_pressed("down"):
		direction += Vector2.DOWN
	if Input.is_action_pressed("left"):
		direction += Vector2.LEFT
	if Input.is_action_pressed("right"):
		direction += Vector2.RIGHT
	
	return self
