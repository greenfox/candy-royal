extends State

#export var speed = 2500
func enter(dict={}):
	pass
	yield(get_tree().create_timer(pawn.stunDurration),"timeout")
	state_machine.transition_to("Walk")
	#start timmer

func physics_update(delta):
	var c = pawn.getInput()
	
	
	var vec3dir = Vector3(c.direction.x,0,c.direction.y).normalized()
	var outspeed = pawn.move_and_slide(vec3dir*delta*pawn.stunnedWalkSpeed + (pawn.gravity))
	outspeed.y = 0
		
	if pawn.translation.y < -10:
		pawn.translation = Vector3(0,10,0)

	if c.fire:
		pawn.fire()
