class_name Player
extends Node

export var pawnScene:PackedScene

signal dataUpdated

var pawn:Node

class PlayerData:
	var username:String = "<loading>"
	var color:Color = Color.gray
	var ready:bool = false
	var score:int = 0
	
var data:PlayerData = PlayerData.new()


func _ready():
	name = "player" + String(get_network_master())
	if is_network_master():
		data = PlayerData.new()
		data.username = NetworkManager.userName
		randomize()
		data.color = Color.from_hsv(randf(),1.0,1.0)
		NetworkManager.connect("peerConnected",self,"sendPlayerData")
		dataUpdated()
	else:
		pass


func sendPlayerData(id):
	rpc_id(id,"setPlayerData",inst2dict(data))

remotesync func setPlayerData(userData):
	data = dict2inst(userData)
	emit_signal("dataUpdated",data)

func readyStatus(button_pressed):
	pass

func dataUpdated():
	rpc("setPlayerData",inst2dict(data))


var isHost:bool setget , getIsHost
func getIsHost()->bool:
	return get_network_master()==1

var isCurrent:bool setget , getIsCurrent
func getIsCurrent()->bool:
	return is_network_master()
	
func unready():
	data.ready = false
	dataUpdated()
	
func ready():
	data.ready = true
	dataUpdated()
	
func scorePoint(value):
	data.score += value
	dataUpdated()

func setColor(value):
	data.color = value
	dataUpdated()

func resetPlayer():
	data.ready = false
	data.score = 0
	dataUpdated()
	
