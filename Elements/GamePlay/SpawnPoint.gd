class_name SpawnPoint
extends Spatial


enum SPAWN_LOCATION {
	UNDEFINED=-1,
	UPPER_LEFT,
	LOWER_RIGHT,
	UPPER_RIGHT,
	LOWER_LEFT
}

export(SPAWN_LOCATION) var spawnPointLocation = SPAWN_LOCATION.UNDEFINED
